var React = require('react');
var {
    Router, Route, IndexRoute
}
= require('react-router');
var Framework = require('./components/Framework.react');

var Header = require('./components/Header');
var mainMenu = require('./components/MainMenu');
var MainMenuSettings = require('./components/MainMenu/components/Settings');
var MainMenuPlugins = require('./components/MainMenu/components/Plugins');
var Message = require('./components/Message');
var Preferences = require('./components/Preferences');
var modal = require('./components/Modal');
var player = require('./components/Player');
var TorrentDashboard = require('./components/TorrentDashboard');

module.exports.framework = new Framework();
module.exports.header = new Header();
module.exports.mainMenu = mainMenu;
module.exports.message = new Message();
module.exports.preferences = new Preferences();
module.exports.modal = modal;
module.exports.player = player;
module.exports.torrentDashboard = new TorrentDashboard();