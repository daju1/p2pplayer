class callbacks
{
    is_dict(obj)
    {
        if (!obj) return false;
        if (Array.isArray(obj)) return false;
        if (obj.constructor != Object) return false;
        return true;
    }
    print_array(arr, s)
    {
        for (var index in arr){
            if (undefined == arr[index])
                s[0] += "undefined";
            else if (this.is_dict(arr[index]))
                this.print_dict(arr[index], s)
            else if (Array.isArray(arr[index]))
            {
                s[0] += "<br>";
                this.print_array(arr[index], s);
            }
            else
                s[0] += arr[index].toString();
            s[0] += "<br>";
        }
    }
    print_dict(h, s)
    {
        for (var key in h){
            s[0] += key.toString() + " -> ";
            if (undefined == h[key])
                s[0] += "undefined";
            else if (Array.isArray(h[key]))
            {
                s[0] += "<br>";
                this.print_array(h[key], s);
            }
            else if (this.is_dict(h[key]))
                this.print_dict(h[key], s)
            else
                s[0] += h[key].toString();
            s[0] += "<br>";
            //if (key == "name") doSomething();
        }
    }
    setState(h)
    {
        //console.log(h);
        var s = [''];
        this.print_dict(h, s);
        var renderRef = document.getElementsByClassName('callbacks')[0];
        renderRef.innerHTML = s[0];
    }
};

module.exports = new callbacks();