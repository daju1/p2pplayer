
var {
    dialog
}
= require('remote');
 
var alt = require('./alt');

var ModalActions = require('./components/Modal/actions');
var MainMenuActions = require('./components/MainMenu/actions');
var PlayerActions = require('./components/Player/actions');
var PlayerContextMenu = require('./components/Player/utils/ContextMenu');
var TorrentActions = require('./actions/torrentActions');
var VolumeActions = require('./components/Player/components/Controls/components/Volume/actions');
var SubtitleActions = require('./components/Player/components/SubtitleText/actions');

var sorter = require('./components/Player/utils/sort');
var parser = require('./components/Player/utils/parser');
var metaParser = require('./components/Player/utils/metaParser');
var filmonUtil = require('./components/Player/utils/filmon');

var supported = require('./utils/isSupported');

var engineStore = require('./stores/engineStore');
var ModalStore = require('./components/Modal/store');

var player = require('./components/Player/utils/player');




var _ = require('lodash');


var webUtil = require('./utils/webUtil');
var routes = require('./routes');
var ls = require('local-storage');


webUtil.disableGlobalBackspace();

var callback_torrent_inited = function()
{
    console.log("callback_torrent_inited");
}

var callback_torrent_got_content = function(files)
{
    console.log("callback_torrent_got_content files = " + files);
    console.log("files.files_total=" + files.files_total);
}
var callback_started_torrent_dashboard = function()
{
    console.log("callback_started_torrent_dashboard");
}
var callback_torrent_ok = function()
{
    console.log("callback_torrent_ok");
}

class p2pPlayerAPI {
    ready()
    {
        console.log(routes);
        routes.framework.componentDidMount();
        routes.mainMenu.mainMenu.componentDidMount();
        routes.mainMenu.settings.componentDidMount();

        routes.modal.modal.componentDidMount();

        //routes.modal.fileStreamSelector.componentDidMount(); //routes.modal.fileStreamSelector.componentDidMount is not a function
        routes.modal.urlAdd.componentDidMount();
        routes.modal.thinking.componentDidMount();
        routes.modal.dashboardMenu.componentDidMount();
        routes.modal.dashboardFileMenu.componentDidMount();
        routes.modal.askRemove.componentDidMount();
        routes.modal.about.componentDidMount();
        routes.modal.plugin.componentDidMount();
        routes.modal.installedPlugin.componentDidMount();
        routes.modal.searchPlugin.componentDidMount();
        routes.modal.searchPlugins.componentDidMount();
        routes.modal.torrentSelector.componentDidMount();
        routes.modal.torrentWarning.componentDidMount();

        routes.player.player.componentDidMount();

        //routes.player.playerHeader.componentDidMount();
        routes.player.playerRender.componentDidMount();
        //routes.player.announcement.componentDidMount();
        //routes.player.subtitleText.componentDidMount();
        routes.player.playerControls.componentDidMount();
        //routes.player.playList.componentDidMount();
        routes.player.menuHoldersSettings.componentDidMount();
        routes.player.settings.componentDidMount();
        //routes.player.subtitleList.componentDidMount();
        //routes.player.castingMenu.componentDidMount();

        //routes.player.components_humanTime.componentDidMount();
        //routes.player.components_tooltip.componentDidMount();
        routes.player.components_progressBar.componentDidMount();
        routes.player.components_volume.componentDidMount();

        routes.torrentDashboard.componentDidMount();
   }

    constructor()
    {
        document.addEventListener("DOMContentLoaded", this.ready);

        routes.framework.componentWillMount();
        //routes.header.componentWillMount();
        routes.mainMenu.mainMenu.componentWillMount();
        routes.mainMenu.plugins.componentWillMount();
        routes.mainMenu.settings.componentWillMount();
        routes.message.componentWillMount();

        //routes.modal.modal.componentWillMount();

        routes.modal.fileStreamSelector.componentWillMount();
        //routes.modal.urlAdd.componentWillMount();
        routes.modal.thinking.componentWillMount();
        //routes.modal.dashboardMenu.componentWillMount();
        //routes.modal.dashboardFileMenu.componentWillMount();
        //routes.modal.askRemove.componentWillMount();
        //routes.modal.about.componentWillMount();
        routes.modal.plugin.componentWillMount();
        routes.modal.installedPlugin.componentWillMount();
        //routes.modal.searchPlugin.componentWillMount();
        //routes.modal.searchPlugins.componentWillMount();
        // D:\node.js\myrepo\tst\p2pPlayer\node_modules\multipass-torrent\node_modules\needle\lib\needle.js:125 Uncaught TypeError: URL must be a string, not undefined
        routes.modal.torrentSelector.componentWillMount();
        routes.modal.torrentWarning.componentWillMount();

        routes.player.player.componentWillMount();

        routes.player.playerHeader.componentWillMount();
        routes.player.playerRender.componentWillMount();
        routes.player.announcement.componentWillMount();
        routes.player.subtitleText.componentWillMount();
        routes.player.playerControls.componentWillMount();
        routes.player.playList.componentWillMount();
        routes.player.settings.componentWillMount();
        routes.player.menuHoldersSettings.componentWillMount();
        routes.player.subtitleList.componentWillMount();
        routes.player.castingMenu.componentWillMount();

        routes.player.components_humanTime.componentWillMount();
        routes.player.components_tooltip.componentWillMount();
        routes.player.components_progressBar.componentWillMount();
        routes.player.components_volume.componentWillMount();

        routes.preferences.componentWillMount();
        //routes.torrentDashboard.componentWillMount();

    }

    open_local_torrent()
    {
        MainMenuActions.openLocal('torrent');
    }
    open_local_video()
    {
        MainMenuActions.openLocal('video');
    }
    open_torrent(fn)
    {
        TorrentActions.addTorrent(fn);
    }

    open_magnet(url)
    {
        TorrentActions.addTorrent(url);
    }

    get_torrent_path()
    {
        var engineState = engineStore.getState(),
            torrent = engineState.torrents[engineState.infoHash];

        if (!torrent)
            return "";

        return torrent.path;
    }

    get_torrent_files_number()
    {
        var engineState = engineStore.getState(),
            torrent = engineState.torrents[engineState.infoHash];

        if (!torrent)
            return -1;

        return torrent.files.length;
    }

    get_torrent_current_progress() {
        var engineState = engineStore.getState(),
            torrent = engineState.torrents[engineState.infoHash];

        if (!torrent)
            return;

        var fileList = [];
        var backColor = '#3e3e3e';
        var progress = torrent.torrent.pieces.downloaded / torrent.torrent.pieces.length;
        console.log("torrent_current_progress="+progress);

        var finished = false;
        if (progress == 1) {
            finished = true;
        }
        torrent.files.forEach( (el, ij) => {
            var fileProgress = Math.round(torrent.torrent.pieces.bank.filePercent(el.offset, el.length) * 100);
            console.log("torrent_current_file id = " + el.fileID);
            console.log("torrent_current_file name = " + el.name);
            console.log("torrent_current_file path = " + el.path);
            console.log("torrent_current_file length = " + el.length);
            console.log("torrent_current_file offset = " + el.offset);

            console.log("torrent_current_fileProgress="+fileProgress);
            console.log("torrent_current_file ij = "+ij);

            var fileFinished = false;
            if (finished || fileProgress >= 100) {
                fileProgress = 100;
                fileFinished = true;
            }
        })
    }

    play_now(file_index)
    {
        var engineState = engineStore.getState(),
            torrent = engineState.torrents[engineState.infoHash];
			
        if (!torrent){
            console.warn("torrent is undefined");
            return;
        }

        var file = torrent.files[file_index];

        if (!file)
        {
            console.warn("no file to play");
            return;
        }

        if (/^win/.test(process.platform)) var pathBreak = "\\";
        else var pathBreak = "/";

        var playlistItem = -1;

        for (var i = 0; i < player.wcjs.playlist.items.count; i++) {
            if (player.wcjs.playlist.items[i].mrl.endsWith('/' + file.fileID)) {
                playlistItem = i;
                break;
            }
        }

        if (playlistItem > -1) {
            player.saveState = {
                idx: playlistItem,
                position: 0
            };
        }

        //this.history.replaceState(null, 'player');

        player.wcjs.play();
    }
    play_playlist_item(playlistItem)
    {
        playlistItem = parseInt(playlistItem);
        if (playlistItem > -1 && playlistItem < player.wcjs.playlist.items.count) {
            player.saveState = {
                idx: playlistItem,
                position: 0
            };
            player.wcjs.playlist.currentItem = playlistItem;
            player.wcjs.play();
        }
    }
    play(){
        if (player.wcjs)
            player.wcjs.play();
    }
    pause(){
        if (player.wcjs)
            player.wcjs.pause();
    }
    stop(){
        if (player.wcjs)
            player.wcjs.stop();
    }
    get_playlist_items_count()
    {
        if (player.wcjs)
            return player.wcjs.playlist.items.count;
        return -1;
    }
    get_playlist_current_item()
    {
        if (player.wcjs)
            return player.wcjs.playlist.currentItem;
        return -1;
    }
    log_playlist()
    {
        if (!player.wcjs)
            return;
        for (var i = 0; i < player.wcjs.playlist.items.count; i++) {
            console.log(player.wcjs.playlist.items[i].mrl);
        }
    }
    get_player_item_desc()
    {
        var player_item_desc = player.itemDesc();
        /*
        URL : ""
        album:""
        artist:""
        artworkURL:""
        copyright:""
        date:""
        description:""
        disabled:false
        duration:7833000
        encodedBy:""
        genre:""
        language:""
        mrl:"http://127.0.0.1:61046/0"
        nowPlaying:""
        parsed:true
        publisher:""
        rating:""
        setting:Object
            byteSize:1467893760
            idx:0
            path:"C:\Users\User\AppData\Local\Temp\p2p-Player\torrent-stream\fdf09d6fd04cf2a1ecad436ee900563ade146861\Podvig Odessi.avi"
            streamID:"0"
            title:"Podvig Odessi"
            torrentHash:"fdf09d6fd04cf2a1ecad436ee900563ade146861"
        title:"Podvig Odessi"
        trackID:""
        trackNumber:""
        */
        return player_item_desc;
    }

    get_player_item_duration()
    {
        var player_item_desc = player.itemDesc();
        return player_item_desc.duration;
    }

    get_player_item_byteSize()
    {
        var player_item_desc = player.itemDesc();
        return player_item_desc.setting.byteSize;
    }

    get_player_item_path()
    {
        var player_item_desc = player.itemDesc();
        return player_item_desc.setting.path;
    }

    get_player_item_torrentHash()
    {
        var player_item_desc = player.itemDesc();
        return player_item_desc.setting.torrentHash;
    }

    get_player_item_title()
    {
        var player_item_desc = player.itemDesc();
        return player_item_desc.title;
    }

    get_player_position()
    {
        return player.wcjs.position;
    }

    set_player_position(pos)
    {
        player.wcjs.position = pos;
    }
    volume_handleMute()
    {
        VolumeActions.handleMute();
    }
    volume_setVolume(v)
    {
        var volume = parseInt(v);
        VolumeActions.setVolume(volume);
    }
    volume_off()
    {
        VolumeActions.mute(true);
    }
    volume_on()
    {
        VolumeActions.mute(false);
    }
    volume_handleMute()
    {
        VolumeActions.handleMute();
    }
    get_volume()
    {
        if (player.wcjs)
            return player.wcjs.volume;
        else if (null != ls('volume'))
            return ls('volume');
        return -1;
    }
    get_volume_mute()
    {
        if (player.wcjs)
            return player.wcjs.mute;
        return false;
    }
    get_audio_track() {
        if (player.wcjs)
            return player.wcjs.audio.track;
        return -1;
    }
    get_audio_track_count() {
        if (player.wcjs)
            return player.wcjs.audio.count;
        return -1;
    }
    set_audio_track(i) {
        var track = parseInt(i);
        PlayerContextMenu.actions.audioTrack(track);
    }
    get_buffering()
    {
        var engineState = engineStore.getState();

        /*if (player.wcjs.state > 1) {
            prebuffering.end();
            return;
        }*/

        var current;

        if (player.wcjs.playlist.currentItem == -1) current = 0;
        else current = player.wcjs.playlist.currentItem;

 
        // get file progress
        var fileSel = player.itemDesc(current).setting.streamID;

        // buffering with WebChimera.js is set in time, not byte length
        // we can never know the real prebuffering percent because we can't know
        // the amount of playback time that a piece holds or the duration of
        // the video, no excuse for the silly logic in the next 3 lines, but
        // it takes into account file lengths, more often then not it's correct
        var file = engineState.torrents[engineState.infoHash].files[fileSel];
        var fileProgress = Math.round(engineState.torrents[engineState.infoHash].torrent.pieces.bank.filePercent(file.offset, file.length) * 100);
        var prebuf = Math.floor( fileProgress * 45 );
        return prebuf;
    }

    torrent_dashboard_open_folder()
    {
        routes.modal.dashboardMenu.openFolder();
    }

    torrent_dashboard_forse_download()
    {
        routes.modal.dashboardMenu.forceDownload();
    }

    torrent_dashboard_copy_magnet()
    {
        routes.modal.dashboardMenu.copyMagnet();
    }

    torrent_dashboard_download_all()
    {
        routes.modal.dashboardMenu.downloadAll();
    }

    torrent_dashboard_pause_all()
    {
        routes.modal.dashboardMenu.pauseAll();
    }

    torrent_dashboard_goto_player()
    {
        routes.modal.dashboardMenu.goToPlayer();
    }

    torrent_dashboard_goto_menu()
    {
        routes.modal.dashboardMenu.goToMenu();
    }

    torrent_dashboard_file_play_now()
    {
        routes.modal.dashboardFileMenu.playNow();
    }

    torrent_dashboard_file_open_file()
    {
        routes.modal.dashboardFileMenu.openFile();
    }

    torrent_dashboard_file_open_folder()
    {
        routes.modal.dashboardFileMenu.openFolder();
    }

    torrent_dashboard_file_copy_stream_url()
    {
        routes.modal.dashboardFileMenu.copyStreamURL();
    }

    torrent_ask_remove_handle_yes()
    {
        routes.modal.askRemove.handleYes();
    }

    torrent_ask_remove_handle_no()
    {
        routes.modal.askRemove.handleNo();
    }

    file_stream_selector_get_content()
    {
        return routes.modal.fileStreamSelector.getContent();
    }

    file_stream_selector_handle_select_file(file)
    {
        routes.modal.fileStreamSelector.handleSelectFile(file);
    }

    file_stream_selector_handle_stream_all()
    {
        routes.modal.fileStreamSelector.handleStreamAll();
    }

    file_stream_selector_handle_stream_file(file)
    {
        routes.modal.fileStreamSelector.handleStreamFile(file);
    }

    torrent_selector_get_content()
    {
        return routes.modal.torrentSelector.getContent();
    }

    torrent_selector_handle_select_folder(key)
    {
        //TODO unfinished
        routes.modal.torrentSelector.handleSelectFolder(key);
    }

    torrent_selector_handle_select_file(file)
    {
        routes.modal.torrentSelector.handleSelectFile(file);
    }

    torrent_warning_handle_close()
    {
        routes.modal.torrentWarning.handleClose();
    }

    torrent_warning_install_plugin(el)
    {
        routes.modal.torrentWarning.installPlugin(el);
    }

    torrent_warning_install_plugin(el)
    {
        routes.modal.torrentWarning.installPlugin(el);
    }

    urladd_handle_url_add(url)
    {
        routes.modal.urlAdd.handleURLAdd(url);
    }

    plugin_open_url(url)
    {
        routes.modal.plugin.openURL(url);	
    }

    plugin_install_plugin(el)
    {
        routes.modal.plugin.installPlugin(el);
    }

    installed_plugin_update_feed_url()
    {
        routes.modal.installedPlugin.updateFeedUrl();		
    }

    installed_plugin_open_url(plugin_url)
    {
        routes.modal.installedPlugin.openURL(plugin_url);
    }

    installed_plugin_plugin_shortcut()
    {
        routes.modal.installedPlugin.pluginShortcut();
    }

    installed_plugin_uninstall_plugin(el)
    {
        routes.modal.installedPlugin.uninstallPlugin(el);
    }

    installed_plugin_play_plugin(el)
    {
        routes.modal.installedPlugin.playPlugin(el);
    }

    installed_plugin_search_plugin(el)
    {
        routes.modal.installedPlugin.searchPlugin(el);
    }

    search_plugin_handle_url_add(url)
    {
        routes.modal.searchPlugin.handleURLAdd(url)
    }

    search_plugins_handle_url_add(url)
    {
        routes.modal.searchPlugins.handleURLAdd(url)
    }

    // mainMenu Settings
    get_mainmenu_settings_state()
    {
        var mainmenu_state = routes.mainMenu.settings.state;
        return mainmenu_state;
    }

    get_player_settings_state()
    {
        var player_state = routes.player.settings.state;
        return player_state;
    }

    // cacheFolder
    get_mainmenu_settings_cache_folder()
    {
        var state = routes.mainMenu.settings.state;
        return state.cacheFolder;		
    }

    set_mainmenu_settings_cache_folder()
    {
        routes.mainMenu.settings.handleCacheFolderFocus();
    }

    handle_mainmenu_settings_clear_cache_folder()
    {
        routes.mainMenu.settings.handleClearCacheFolder();
    }

    // downloadFolder
    get_mainmenu_settings_download_folder()
    {
        var state = routes.mainMenu.settings.state;
        return state.downloadFolder;
    }

    set_mainmenu_settings_download_folder()
    {
        routes.mainMenu.settings.handleDownloadFocus();
    }

    handle_mainmenu_settings_clear_download_folder()
    {
        routes.mainMenu.settings.handleClearDownloadFolder();
    }

    // downloadType 'Player' 'Dashboard'
    get_mainmenu_settings_download_type()
    {
        var state = routes.mainMenu.settings.state;
        return state.downloadType;
    }

    handle_mainmenu_settings_download_type(direction)
    {
        routes.mainMenu.settings.handleDownloadType(direction);
    }

    // removeLogic 'Always Ask' 'Always Remove' 'Always Keep'
    get_mainmenu_settings_remove_logic()
    {
        var state = routes.mainMenu.settings.state;
        return state.removeLogic;
    }

    handle_mainmenu_settings_remove_logic(direction)
    {
        return routes.mainMenu.settings.handleRemoveLogic(direction);
    }

    // AudioTrack
    handle_player_settings_audio_track(direction)
    {
        routes.player.settings.handleAudioTrack(direction);
    }

    // AudioChannel
    handle_player_settings_audio_channel(direction)
    {
        routes.player.settings.handleAudioChannel(direction);
    }

    // peers
    get_max_peers()
    {
        return routes.mainMenu.settings.state.peers;		
    }

    set_max_peers(newValue)
    {
        return routes.mainMenu.settings.handlePeersBlur(newValue);
    }

    handle_max_peers(direction)
    {
        return routes.mainMenu.settings.handlePeers(direction);
    }

    // port
    get_peer_port()
    {
        return routes.mainMenu.settings.state.port;
    }

    set_peer_port(newValue)
    {
        return routes.mainMenu.settings.handlePortBlur(newValue);
    }

    handle_peer_port(direction)
    {
        return routes.mainMenu.settings.handlePort(direction);
    }

    // bufferSize
    get_buffer_size()
    {
        return routes.mainMenu.settings.state.bufferSize;
    }

    set_buffer_size(newValue)
    {
        return routes.mainMenu.settings.handleBufferSizeBlur(newValue);
    }

    handle_buffer_size(direction)
    {
        return routes.mainMenu.settings.handleBufferSize(direction);
    }

    // speedPulsing
    get_speed_pulsing()
    {
        return routes.mainMenu.settings.state.speedPulsing;
    }

    set_speed_pulsing(toggled)
    {
        toggled = parseInt(toggled);
        routes.mainMenu.settings.handlePulsingToggle(toggled)
    }

    // subtitles 
    player_subtitle_list_get_items()
    {
        return routes.player.subtitleList.getItems();
    }

    player_subtitle_list_select(idx){
        var item = idx;
        var itemId = item;
        routes.player.subtitleList.select(idx, item, itemId);
    }

    player_subtitle_list_select_internal(idx){
        var item = idx;
        var itemId = item;
        routes.player.subtitleList.selectInternal(idx, item, itemId);
    }

    player_subtitle_list_get_internal_subs()
    {
        return routes.player.subtitleList.getInternalSubs();
    }

    player_subtitle_list_remove_subtitles()
    {
        player_subtitle_list_select(-1);
        player_subtitle_list_select_internal(-1);
    }

    player_subtitle_list_get_all_items()
    {
        var items = routes.player.subtitleList.getItems();
        var internal_items = routes.player.subtitleList.getInternalSubs();
        return items + internal_items;
    }

    find_subtitles()
    {
        var itemDesc = player.itemDesc();

        var itemDesc = itemDesc.setting;

        if (!itemDesc)
        {
            console.warn("!itemDesc");
            return;
        }

        if (itemDesc.subtitles) {
            ControlActions.settingChange({ foundSubs: true });
            SubtitleActions.foundSubs(itemDesc.subtitles, false);
        } else if (itemDesc.path && (!ls.isSet('findSubs') || ls('findSubs')))
            SubtitleActions.findSubs(itemDesc);
    }

    test_filmon()
    {
        var feed = filmonUtil.groups();

        if (!ls('adultContent'))
            feed = _.filter(feed, el => { return ['BIKINI BABE', 'PARTY TV'].indexOf(el.name) == -1 })

        var ij = feed.length-1;
        routes.mainMenu.plugins.openFilmonPlugin(feed, ij);
        var targetPlugin = feed[ij];

        routes.modal.plugin.installPlugin(targetPlugin);
        routes.modal.installedPlugin.playPlugin(targetPlugin);
    }
}

module.exports.api = new p2pPlayerAPI();
module.exports.routes = routes;