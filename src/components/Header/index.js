var React = require('react');
var {
    History
}
= require('react-router');
var HeaderStore = require('./store');
var HeaderActions = require('./actions');
var player = require('../Player/utils/player');
var cb = require("./callback");

module.exports = React.createClass({

    mixins: [History],

    getInitialState() {
        return {
            maximized: false,
            minimized: false,
            view: 'dashboard',
            title: ''
        };
    },
    componentWillMount() {
        HeaderStore.listen(this.update);
        this.history.listen(this.updatehistory);
        player.events.on('windowTitleUpdate', this.setTitle);
    },
    componentWillUnmount() {
        HeaderStore.unlisten(this.update);
        this.history.unlisten(this.updatehistory);
        player.events.removeListener('windowTitleUpdate', this.setTitle);
    },
    updatehistory(n, location) {
        if (location.location.pathname)
            cb.setState(this, {
                view: (location.location.pathname.substr(1) === '') ? 'dashboard' : location.location.pathname.substr(1)
            });
    },
    setTitle(title) {
        cb.setState(this, {
            title: title
        });
    },
    update() {
        if (true) {

            var headerState = HeaderStore.getState();

            cb.setState(this, {
                maximized: headerState.maximized,
                minimized: headerState.minimized
            });
        }
    },
    render() {
        return '';/*(
            <header className={this.state.view}>
                <div className={'controls ' + process.platform}>
                    <div className="close" onClick={HeaderActions.close}>
                        <i className="ion-ios-close-empty"/>
                    </div>
                    <div className="toggle" onClick={HeaderActions.toggleMaximize}>
                        <i/>
                        <i/>
                    </div>
                    <div className="minimize" onClick={HeaderActions.toggleMinimize}>
                        <i/>
                    </div>
                </div>
                <div className="windowTitle">
                    {this.state.title}
                </div>
            </header>
        );*/
    }
});