var cb = require("../../callbacks");
var readableSize = require("./readablesize");
class callback
{

    setTorrentState(torrent)
    {
        if (!torrent)
            return;

        var fileList = [];
        var torrent_progress = torrent.torrent.pieces.downloaded / torrent.torrent.pieces.length;
        var torrent_finished = false;
        if (torrent_progress == 1) {
            torrent_finished = true;
        }

        var torrent_peers = torrent.swarm.wires && torrent.swarm.wires.length ? torrent.swarm.wires.length : 0;
        //console.log("Peers " + torrent_peers);

        var torrent_speed = (torrent_finished ? readableSize(torrent.swarm.uploadSpeed) : readableSize(torrent.swarm.downloadSpeed));
        //console.log("Speed " + torrent_speed);

        var torrent_uploaded = readableSize(torrent.swarm.uploaded);
        //console.log("Uploaded " + torrent_uploaded);

        var torrent_downloaded = torrent_finished ? readableSize(torrent.total.length) : readableSize(torrent.swarm.downloaded);
        //console.log( "Downloaded: " + torrent_downloaded);

        var torrent_total_length = readableSize(torrent.total.length);
        //console.log("torrent_total_length " + torrent_total_length);

        var torrent_progress = (( torrent.torrent.pieces.downloaded / torrent.torrent.pieces.length ) * 100);
        //console.log("Progress " + torrent_progress);

        torrent.files.forEach( (el, ij) => {
            var fileProgress = Math.round(torrent.torrent.pieces.bank.filePercent(el.offset, el.length) * 100);

            var fileFinished = false;
            if (torrent_finished || fileProgress >= 100) {
                fileProgress = 100;
                fileFinished = true;
            }

            //console.log("fileProgress " + fileProgress);
            //console.log("fileFinished " + fileFinished);

            //console.log("Selected " + el.selected);

            var downloaded_part_of_file = fileFinished ? readableSize(el.length) : readableSize(el.length * torrent.torrent.pieces.bank.filePercent(el.offset, el.length));
            var size_of_file = readableSize(el.length);

            //console.log("Downloaded " + downloaded_part_of_file + " from " + size_of_file);

            var file_state = {
                progress: fileProgress,
                finished: fileFinished,
                selected: el.selected,
                downloaded : downloaded_part_of_file,
                size : size_of_file,
                name : el.name,
                path : el.path,
                fileID : el.fileID,
                offset : el.offset
            };

            fileList.push(file_state);
        });

        var torrent_state = {
             progress : torrent_progress,
             finished : torrent_finished,
             peers : torrent_peers,
             speed : torrent_speed,
             uploaded : torrent_uploaded,
             downloaded : torrent_downloaded,
             total_length : torrent_total_length,
             torrent_progress : torrent_progress,
             files : fileList
        };

        cb.setState(torrent_state);
    }

    setState(component, h)
    {
        cb.setState(h);
        for (var key in h){
            if (component.state)
                component.state[key] = h[key];
            //console.log(key + " -> " + h[key]);
            if ('torrents' == key)
            {
                var torrents = h[key];
                for (var torrent_key in torrents)
                {
                    var torrent = torrents[torrent_key];
                    //console.log("torrent_key" + torrent_key);
                    //console.log(torrent);

                    this.setTorrentState(torrent);
                } 
            }
        }
    }
};

module.exports = new callback();