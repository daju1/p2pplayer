var cb = require("../../../callbacks");
class callback
{
    setState(component, h)
    {
        cb.setState(h);
        for (var key in h){
           if (component.state)
                component.state[key] = h[key];

            //console.log(key + " -> " + h[key]);

            if ('event_playing' == key && "true" == h[key])
            {
                 console.log("Playing player");
            }
            if ('event_paused' == key && "true" == h[key])
            {
                 console.log("Paused player");
            }
            if ('event_stopped' == key && "true" == h[key])
            {
                 console.log("Stopped player");
            }
            if ('event_mediaChanged' == key && "true" == h[key])
            {
                 console.log("mediaChanged player");
            }
            if ('event_ended' == key && "true" == h[key])
            {
                 console.log("Playback ended");
            }
            if ('event_error' == key)
            {
                 console.log("Player encountered an error");
                 var itemDesc = h[key];
                 console.log(itemDesc);
            }
        }
    }
};

module.exports = new callback();