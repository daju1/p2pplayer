var React = require('react');

var SettingsPanel = require('../Settings.react');
var PlayerActions = require('../../actions');
var VisibilityStore = require('../Visibility/store');
var VisibilityActions = require('../Visibility/actions');
var cb = require("./callback");


module.exports = React.createClass({

    getInitialState() {
        var visibilityState = VisibilityStore.getState();
        return {
            open: false,
            uiHidden: visibilityState.uiHidden,
        }
    },
    componentWillMount() {
        VisibilityStore.listen(this.update);
    },

    componentWillUnmount() {
        VisibilityStore.unlisten(this.update);
    },
    
    componentDidMount() {

    },

    update() {
//        console.log('settings holder update');
        if (true) {
            var visibilityState = VisibilityStore.getState();
            cb.setState(this, {
                open: visibilityState.settings,
                uiHidden: visibilityState.uiHidden
            });
        }
    },

    close() {
        PlayerActions.openSettings(false);
    },

    render() {
        return '';/*(
            <div className={this.state.uiHidden ? 'playlist-container' : this.state.open ? 'playlist-container show' : 'playlist-container'}>
                <div className="playlist-controls" / >
                <div className="playlist-holder settings-holder" style={{marginLeft: '0', height: '100%', textAlign: 'center'}}>
                    <SettingsPanel />
                </div> 
            </div>
        );*/
    }

});