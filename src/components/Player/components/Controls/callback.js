var cb = require("../../../../callbacks");
class callback
{
    setState(component, h)
    {
        cb.setState(h);
        for (var key in h){
            //console.log(key + " -> " + h[key]);
            if (component.state)
                component.state[key] = h[key];

            if ('subtitlesOpen' == key)
            {
                console.log("Opening of subtitles " + (h[key]).toString());
            }
        }
    }
};

module.exports = new callback();