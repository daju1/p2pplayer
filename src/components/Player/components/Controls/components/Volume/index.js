﻿var React = require('react');
var PureRenderMixin = require('react-addons-pure-render-mixin');
var ls = require('local-storage');
var VolumeStore = require('./store');
var VolumeActions = require('./actions');
var cb = require("./callback");


module.exports = React.createClass({

    mixins: [PureRenderMixin],

    getInitialState() {
        return {
            volume: ls.isSet('volume') ? ls('volume') : 100,
            muted: false
        }
    },
    componentWillMount() {
        VolumeStore.listen(this.update);
        this.refs_ = {};
    },
    componentDidMount() {
        var volume_slider = this.refs['volume-slider'];
        if (!volume_slider)
            volume_slider = document.getElementsByClassName('vol-slide')[0];

        if (volume_slider){
            this.refs_['volume-slider'] = volume_slider;

            VolumeActions.settingChange({
                volumeSlider: this.refs_['volume-slider']
            });
        }
        /*this.refs_['volume-slider'].addEventListener('immediate-value-change', VolumeActions.handleVolume);
        this.refs_['volume-slider'].addEventListener('mousedown', VolumeActions.volumeDragStart);
        this.refs_['volume-slider'].addEventListener('mouseup', VolumeActions.volumeDragStop);*/
//        var volumeIndex = this.refs['volume-slider'].refs['track'].lastChild;
//        var volumeClass = volumeIndex.className.replace(' volume-hover', '');
//        volumeIndex.className = volumeClass + ' volume-hover';
    },
    componentWillUnmount() {
        VolumeStore.unlisten(this.update);
    },
    update() {
        if (true) {
            var volumeState = VolumeStore.getState();
            cb.setState(this, {
                volume: volumeState.volume,
                muted: volumeState.muted
            });
        }
    },
    setVolume(t) {
        //var t = document.querySelector('.vol-slide').immediateValue;
        VolumeActions.setVolume(t);
    },
    render() {
        return '';/*(
            <div>
                <paper-icon-button onClick={VolumeActions.handleMute} icon={ 'av:' + (this.state.muted ? 'volume-off' : this.state.volume <= 0 ? 'volume-mute' : this.state.volume <= 120 ? 'volume-down' : 'volume-up') } className="volume-button" noink={true} />
                <paper-slider onClick={this.setVolume} className="vol-slide" name="volume-slider" ref="volume-slider" min="0" max="200" steps="1" value={this.state.muted ? 0 : this.state.volume} noink={true}/>
            </div>
        );*/
    }
});