var cb = require("../../../callbacks");
class callback
{
    setState(component, h)
    {
        cb.setState(h);
        for (var key in h){
            //console.log(key + " -> " + h[key]);
            if (component.state)
                component.state[key] = h[key];

            if ('text' == key && "Opening Media" == h[key])
            {
                 console.log("Opening Media ");
            }
            if ('text' == key && "Prebuffering 0%" == h[key])
            {                  
                 console.log("Start of prebuffering");
            }
            if ('text' == key && "Prebuffering 100%" == h[key])
            {
                 console.log("End of prebuffering");
            }
            if ('text' == key && "Buffering 100%" == h[key])
            {
                 console.log("End of buffering");
            }
        }
    }
};

module.exports = new callback();