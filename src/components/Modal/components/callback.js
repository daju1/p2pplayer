var cb = require("../../../callbacks");
class callback
{
    setState(component, h)
    {
        cb.setState(h);
        for (var key in h){
            if (component.state)
                component.state[key] = h[key];
            //console.log(key + " -> " + h[key]);
        }
    }
};

module.exports = new callback();