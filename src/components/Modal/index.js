var React = require('react');
var PureRenderMixin = require('react-addons-pure-render-mixin');

var ModalStore = require('./store');
var ModalActions = require('./actions');

var FileStreamSelector = require('./components/fileStreamSelector');
var UrlAdd = require('./components/URLadd');
var Thinking = require('./components/Thinking');
var DashboardMenu = require('./components/dashboardMenu');
var DashboardFileMenu = require('./components/dashboardFileMenu');
var AskRemove = require('./components/askRemove');
var About = require('./components/about');
var Plugin = require('./components/Plugin');
var InstalledPlugin = require('./components/InstalledPlugin');
var SearchPlugin = require('./components/SearchPlugin');
var SearchPlugins = require('./components/SearchPlugins');
var TorrentSelector = require('./components/torrentSelector');
var TorrentWarning = require('./components/torrentWarning');
var cb = require("./callback");


const Modal = React.createClass({

    mixins: [PureRenderMixin],

    getInitialState() {

        var modalState = ModalStore.getState();

        return {
            Thinking: modalState.thinking,
            modalIsOpen: modalState.open,
            type: modalState.type,
            data: modalState.data
        };
    },

    componentDidMount() {
        ModalStore.listen(this.update);
    },

    componentWillUnmount() {
        ModalStore.unlisten(this.update);
    },

    update() {
        if (true) {

            var modalState = ModalStore.getState();

            cb.setState(this, {
                modalIsOpen: modalState.open,
                data: modalState.data,
                type: modalState.type,
                Thinking: modalState.thinking
            });
        }
    },

    openModal() {
        cb.setState(this, {
            modalIsOpen: true
        });
    },

    closeModal() {
        cb.setState(this, {
            modalIsOpen: false
        });
    },

    /*getContents() {
        switch (this.state.type) {
            case 'URLAdd':
                return <URLContents />;
                break;
            case 'fileSelctor':
                return <FileStreamSelector />;
                break;
            case 'thinking':
                return <Thinking />;
                break;
            case 'dashboardMenu':
                return <DashboardMenu />;
                break;
            case 'dashboardFileMenu':
                return <DashboardFileMenu />;
                break;
            case 'askRemove':
                return <AskRemove />;
                break;
            case 'about':
                return <About />;
                break;
            case 'plugin':
                return <Plugin />;
                break;
            case 'installedPlugin':
                return <InstalledPlugin />;
                break;
            case 'searchPlugin':
                return <SearchPlugin />;
                break;
            case 'searchPlugins':
                return <SearchPlugins />;
                break;
            case 'torrentSelector':
                return <TorrentSelector />;
                break;
            case 'torrentWarning':
                return <TorrentWarning />;
                break;
        }
    },*/

    render() {
        return '';/*(
            <div style={{width: '0px', height: '0px'}}>
                {this.getContents()}
            </div>
        );*/
    }
});

module.exports.modal = new Modal();

module.exports.fileStreamSelector = new FileStreamSelector();
module.exports.urlAdd = new UrlAdd();
module.exports.thinking = new Thinking();
module.exports.dashboardMenu = new DashboardMenu();
module.exports.dashboardFileMenu = new  DashboardFileMenu();
module.exports.askRemove = new AskRemove();
module.exports.about = new About();
module.exports.plugin = new Plugin();
module.exports.installedPlugin = new InstalledPlugin();
module.exports.searchPlugin = new SearchPlugin();
module.exports.searchPlugins = new SearchPlugins();
module.exports.torrentSelector = new TorrentSelector();
module.exports.torrentWarning = new TorrentWarning();
