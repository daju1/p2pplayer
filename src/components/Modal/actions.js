var alt = require('../../alt');

module.exports = alt.generateActions('data', 'close', 'open', 'thinking', 'metaUpdate', 'fileSelector', 'setIndex', 'shouldExit', 'plugin', 'installedPlugin', 'searchPlugin', 'searchPlugins', 'torrentSelector', 'torrentWarning');